# Fusion Wallet

### Syscoin asset wallet

## Instructions

- Clone this repository.
- Run `npm install`.
- Copy syscoin dependencies (syscoind and syscoin-cli) into `extra` folder. If you skip this step, you will receive an error during wallet initialization.

### To run in dev mode

- Run `npm run dev`.

### To build binaries

- Run `npm run package` to build the version for the OS you're currently running. Run `npm run package-all` if you want to attempt building binaries for all platforms.
